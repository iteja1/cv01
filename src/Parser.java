import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class Parser {
    private ArrayList<String> OPERATORS;

    public ArrayList<String> lexems;
    public Stack<Token> stack;

    public Parser() {
        OPERATORS = new ArrayList<String>();
        OPERATORS.add("+");
        OPERATORS.add("-");
        OPERATORS.add("*");
        OPERATORS.add("/");

        stack = new Stack<Token>();
        lexems = new ArrayList<>();
    }

    public void vytvorLexem(String aritmetic) {
        for (String lexem : aritmetic.split(" ")) {
            if (!lexem.trim().isEmpty()) lexems.add(lexem);
        }
    }

    public void vytvorTokens() {
        Collections.reverse(lexems);

        for (String lexema : lexems) {
            if (OPERATORS.contains(lexema)) {
                stack.add(new Token(lexema, "operator"));
            } else {
                stack.add(new Token("cislo", lexema));
            }
        }
    }

    public double calculate() {
        Vyraz vyraz = nactiVyraz();
        return vyraz.calculate();
    }

    private Vyraz nactiVyraz() {
        Vyraz vyraz = new Vyraz();

        Term t1 = nactiTerm();
        vyraz.terms.put(t1, "");

        while (!stack.empty()) {
            Token t = stack.peek();
            if ("+".equals(t.typ) || "-".equals(t.typ)) {
                String operator = t.typ;
                stack.pop();

                Term tn = nactiTerm();
                vyraz.terms.put(tn, operator);
            } else {
                break;
            }
        }
        return vyraz;
    }

    private Term nactiTerm() {
        Term term = new Term();

        Faktor l = nactiFaktor();
        term.l = l;

        while (!stack.isEmpty()) {
            Token t = stack.peek();
            if ("*".equals(t.typ) || "/".equals(t.typ)) {
                String operator = t.typ;
                stack.pop();

                Faktor p = nactiFaktor();
                term.p = p;
                term.operator = operator;
            } else {
                break;
            }
        }
        return term;
    }

    private Faktor nactiFaktor() {
        Token t = stack.pop();

        Faktor faktor = new Faktor();
        if ("cislo".equals(t.typ)) {
            faktor.hodnota = t.hodnota;
        }
        return faktor;
    }
}
