public class Term {
    public Faktor l;
    public Faktor p;
    public String operator;

    public Term() {
    }

    double calculate() {
        double hodnota = 0;

        if (l != null) {
            hodnota = Integer.parseInt(l.hodnota);
        }
        if (p != null) {
            if (operator.equals("*")) {
                hodnota *= Integer.parseInt(p.hodnota);
            } else {
                hodnota /= Integer.parseInt(p.hodnota);
            }

        }
        return hodnota;
    }
}
