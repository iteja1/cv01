import java.util.HashMap;
import java.util.Map;

public class Vyraz {
    public Map<Term, String> terms = new HashMap<>();


    double calculate() {
        double hodntoa = 0;

        for (Map.Entry<Term, String> entry : terms.entrySet()) {
            String operator = entry.getValue();
            double tHodnota = entry.getKey().calculate();

            if (operator.equals("+"))  hodntoa +=tHodnota;
            else if(operator.equals("-")) hodntoa -= tHodnota;
            else hodntoa +=tHodnota;
        }
        return hodntoa;
    }
}
