public class Main {

    public static void main(String[] args) {
        String vyraz = "2 + 3 * 4 - 2 / 6";

        Parser p = new Parser();
        p.vytvorLexem(vyraz);
        System.out.println("Lexemy");
        for (String lexem : p.lexems) {
            System.out.println(lexem);
        }

        System.out.println("Tokeny");
        p.vytvorTokens();

        for (Token token :p.stack ){
            System.out.println(token.typ + "  " + token.hodnota);
        }

        System.out.println("Vypocet");
        System.out.println(p.calculate());

    }
}
